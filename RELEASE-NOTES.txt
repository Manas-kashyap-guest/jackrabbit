Release Notes -- Apache Jackrabbit -- Version 2.14.6

Introduction
------------

This is Apache Jackrabbit(TM) 2.14.6, a fully compliant implementation of the
Content Repository for Java(TM) Technology API, version 2.0 (JCR 2.0) as
specified in the Java Specification Request 283 (JSR 283).

Apache Jackrabbit 2.14.6 is a patch release that contains fixes and
improvements over Jackrabbit 2.14. Jackrabbit 2.14.x releases are
considered stable and targeted for production use.
  
Changes in Jackrabbit 2.14.6
----------------------------

Bug

    [JCR-4093] - IndexRule are meant to be applied based on both primaryType and minin type based inheritance. Currently it appears that only primaryType based inheritance is working
    [JCR-4291] - FileInputStream for workspace.xml not closed in RepositoryConfig.loadWorkspaceConfig(File)
    [JCR-4317] - davex remoting fails for non-ASCII characters in node names
    [JCR-4324] - NPE on Version.getLinearPredecessor() implementation

Improvement

    [JCR-3211] - Support HTTP proxy in SPI2DAV
    [JCR-4253] - RepositoryConfig: add some handling for mkdir failure
    [JCR-4292] - davex: preserve cause in exceptions and log affected URI

Task

    [JCR-4254] - Update Logback version to >= 1.2.0, SLF4J accordingly
    [JCR-4256] - create announcement mail template for releases
    [JCR-4261] - webapp: align jsons-simple dependencies internally and with oak
    [JCR-4262] - jcr-server: align org.apache.felix.scr.annotations with oak
    [JCR-4263] - jcr-server, jackrabbit-bundle: align org.osgi dependencies with oak
    [JCR-4264] - jackrabbit-standalone: align commons-cli dependency with oak
    [JCR-4272] - Upgrade surefire and failsafe plugins to 2.21.0
    [JCR-4288] - Upgrade tika-parsers dependency to 1.18
    [JCR-4290] - remove unused commons-codec dependency
    [JCR-4293] - jackrabbit-core: observation tests should not rely on mix:lockable mixin type
    [JCR-4294] - TCK tests should pass on repositories without locking support
    [JCR-4296] - Upgrade httpmime dependency to 4.5.5
    [JCR-4302] - BTreeManager: fix Eclipse compiler error
    [JCR-4304] - update Jetty to supported version 9.2.*
    [JCR-4307] - Update animal-sniffer-maven-plugin to 1.16
    [JCR-4318] - Update failsafe and surefire plugin versions to 2.22.0
    [JCR-4320] - Update spotbugs plugin to 3.1.5
    [JCR-4321] - Update maven plugins from org.apache.maven.plugins
    [JCR-4322] - Consistent use of log4j versions
    [JCR-4323] - webapp: update Tomcat dependency to 8.5.32
    [JCR-4326] - Update aws java sdk version to 1.11.330 (consistent with Oak)
    [JCR-4327] - Update httpcore dependency to 4.4.10
    [JCR-4331] - Update httpclient dependency to 4.5.6
    [JCR-4332] - Update httpmime dependency to 4.5.6
    [JCR-4333] - Update javax.transaction dependency to 1.3

Sub-task

    [JCR-4280] - code coverage checks fail on Java 10
    [JCR-4306] - switch to findbugs replacement that is still maintained (spotbugs)
    [JCR-4338] - avoid use of javax.rmi.PortableRemoteObject (removed in Java 11)


For more detailed information about all the changes in this and other
Jackrabbit releases, please see the Jackrabbit issue tracker at

    https://issues.apache.org/jira/browse/JCR

Release Contents
----------------

This release consists of a single source archive packaged as a zip file.
The archive can be unpacked with the jar tool from your JDK installation.
See the README.txt file for instructions on how to build this release.

The source archive is accompanied by SHA1 and SHA512 checksums and a
PGP signature that you can use to verify the authenticity of your
download. The public key used for the PGP signature can be found at
https://www.apache.org/dist/jackrabbit/KEYS.

About Apache Jackrabbit
-----------------------

Apache Jackrabbit is a fully conforming implementation of the Content
Repository for Java Technology API (JCR). A content repository is a
hierarchical content store with support for structured and unstructured
content, full text search, versioning, transactions, observation, and
more.

For more information, visit http://jackrabbit.apache.org/

About The Apache Software Foundation
------------------------------------

Established in 1999, The Apache Software Foundation provides organizational,
legal, and financial support for more than 140 freely-available,
collaboratively-developed Open Source projects. The pragmatic Apache License
enables individual and commercial users to easily deploy Apache software;
the Foundation's intellectual property framework limits the legal exposure
of its 3,800+ contributors.

For more information, visit http://www.apache.org/

Trademarks
----------

Apache Jackrabbit, Jackrabbit, Apache, the Apache feather logo, and the Apache
Jackrabbit project logo are trademarks of The Apache Software Foundation.
